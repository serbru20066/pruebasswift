//
//  ViewController.swift
//  pruebita
//
//  Created by bruno2 on 21/08/14.
//  Copyright (c) 2014 Altimea. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
                            
    @IBOutlet weak var tableView: UITableView!
    
    var arrPersonas:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
         arrPersonas = ["Bruno","Pepe","Mama"]
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return arrPersonas.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        
        cell.textLabel.text = arrPersonas[indexPath.row]
        cell.backgroundColor = UIColor.blackColor()
        cell.textLabel.textColor = UIColor.orangeColor()
        
        
        return cell
        
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        var alert = UIAlertController(title: "Alert", message: arrPersonas[indexPath.row], preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: arrPersonas[indexPath.row], style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    

}

